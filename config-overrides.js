const { override } = require('customize-cra')

const fixEslintLoaderOptions = fixCallback => webpackConfig => {
    let eslintOptionsCount = 0

    webpackConfig.module.rules.forEach(({ use = [] }) => {
        use.forEach(({ options, loader }) => {
            if (loader && loader.includes('eslint-loader') && options && options.useEslintrc !== null) {
                eslintOptionsCount += 1

                fixCallback(options)
            }
        })
    })

    if (eslintOptionsCount === 0) {
        throw new Error('"eslint-loader" doesnt found in webpack config')
    } else if (eslintOptionsCount > 1) {
        throw new Error('"eslint-loader" found few times in webpack config')
    }

    return webpackConfig
}

module.exports = override(
    fixEslintLoaderOptions(eslintOptions => {
        // Надоело, что компиляция постоянно падает на ошибках типа отступ не тот и кавычка не такая
        // Теперь будут просто варнинги
        eslintOptions.emitWarning = true
        eslintOptions.failOnWarning = false
    })
)

import React, { useRef, useEffect } from 'react'
import { clearAllBodyScrollLocks, disableBodyScroll } from 'body-scroll-lock'
import { Link } from 'react-router-dom'
import { useIntl } from 'react-intl'

import {
    IconDrawerClose,
    IconArrowForward,
    IconSearchByType,
    IconSearchByKeyword,
    IconSearchByLocation,
} from '../../resources/icons'

import IconMarker from './IconMarker'
import './Drawer.scss'


/* eslint react/prop-types: ["off"] */
const Drawer = ({ closeDrawer }) => {
    const intl = useIntl()
    const drawerRef = useRef()

    useEffect(() => {
        disableBodyScroll(drawerRef.current)
        return () => {
            clearAllBodyScrollLocks()
        }
    })

    return (
        <div ref={drawerRef} className="drawer-wrapper">
            <div className="icon-marker-wrapper">
                <IconMarker />
            </div>
            <div className="head">
                <div className="close-button" onClick={closeDrawer} aria-hidden>
                    <IconDrawerClose />
                </div>
            </div>
            <div className="menu-wrapper">
                <Link to="/station_search_by_type" className="menu-item" onClick={closeDrawer}>
                    <div className="menu-iIcon-wrapper">
                        <IconSearchByType />
                    </div>
                    <div className="menu-item-title">
                        {intl.formatMessage({
                            id: 'widgets.drawer.menu.searchByType',
                        })}
                    </div>
                    <IconArrowForward />
                </Link>
                <Link to="/station_search_by_keyword" className="menu-item" onClick={closeDrawer}>
                    <div className="menu-iIcon-wrapper">
                        <IconSearchByKeyword />
                    </div>
                    <div className="menu-item-title">
                        {intl.formatMessage({
                            id: 'widgets.drawer.menu.searchByKeyword',
                        })}
                    </div>
                    <IconArrowForward />
                </Link>
                <Link to="/station_search_by_location" className="menu-item" onClick={closeDrawer}>
                    <div className="menu-iIcon-wrapper">
                        <IconSearchByLocation />
                    </div>
                    <div className="menu-item-title">
                        {intl.formatMessage({
                            id: 'widgets.drawer.menu.searchByLocation',
                        })}
                    </div>
                    <IconArrowForward />
                </Link>
            </div>
        </div>
    )
}

export default Drawer

import React, { useState, useCallback, useEffect } from 'react'
import { connect } from 'react-redux'

import { getLocale } from '../../store/commonSlice'
import { IconLocaleBorder, IconDrawer } from '../../resources/icons'
import Drawer from '../Drawer'
import { LANG_MAP } from '../../resources/lang'

import LocaleDialog from './LocaleDialog'
import './Header.scss'


/* eslint react/prop-types: ["off"] */
const Header = ({ locale, path }) => {
    // Lang dialog
    const [localeDialogOpened, setLocaleDialogOpened] = useState(false)

    const handleOpenLocaleDialog = useCallback(() => {
        setLocaleDialogOpened(true)
    }, [setLocaleDialogOpened])

    const handleCloseLocaleDialog = useCallback(() => {
        setLocaleDialogOpened(false)
    }, [setLocaleDialogOpened])

    // Drawer
    const [drawerOpened, setDrawerOpened] = useState(false)
    const openDrawer = useCallback(() => {
        setDrawerOpened(true)
    }, [setDrawerOpened])

    const closeDrawer = useCallback(() => {
        setDrawerOpened(false)
    }, [setDrawerOpened])

    // Открываем меню для пустой главной страницы
    useEffect(() => {
        if (path === '/') {
            setDrawerOpened(true)
        }
    }, [path, setDrawerOpened])

    return (
        <>
            <div className="header-menu-wrapper">
                <div onClick={openDrawer} className="menu-drawer" aria-hidden>
                    <IconDrawer />
                </div>

                <div onClick={handleOpenLocaleDialog} className="menu-locale" aria-hidden>
                    <IconLocaleBorder />
                    <div className="locale-label">
                        {LANG_MAP[locale].shortLabel.toUpperCase()}
                    </div>
                </div>
            </div>

            <LocaleDialog opened={localeDialogOpened} handleCloseDialog={handleCloseLocaleDialog} />
            {drawerOpened && <Drawer opened={drawerOpened} closeDrawer={closeDrawer} />}
        </>
    )
}

export default connect(
    state => ({
        locale: getLocale(state),
        path: state.router.location.pathname,
    }),
)(Header)

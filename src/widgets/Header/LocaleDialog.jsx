import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { useIntl } from 'react-intl'
import map from 'lodash-es/map'

import { LANG_MAP } from '../../resources/lang'
import { getLocale, setLocale } from '../../store/commonSlice'
import { Dialog, DialogLineCheckbox } from '../../components/Dialog'
import { IconFlag } from '../../resources/icons'


const propTypes = {
    opened: PropTypes.bool.isRequired,
    handleCloseDialog: PropTypes.func.isRequired,
}

/* eslint react/prop-types: ["error", { ignore: [locale, setLocale] }] */
const LocaleDialogBody = ({ opened, handleCloseDialog, locale, setLocale }) => {
    const intl = useIntl()

    const selectLangHandler = useCallback((locale) => {
        setLocale(locale)
        handleCloseDialog()
    }, [setLocale, handleCloseDialog])

    if (!opened) {
        return null
    }

    return (
        <Dialog
            opened
            handleCloseDialog={handleCloseDialog}
            title={intl.formatMessage({ id: 'pages.stationForecasts.title.menu.chooseLang' })}
        >
            {
                map(LANG_MAP, lang => (
                    <DialogLineCheckbox
                        key={lang.locale}
                        type="radio"
                        checked={lang.locale === locale}
                        title={lang.label}
                        handleArg={lang.locale}
                        handleCheck={selectLangHandler}
                    >
                        <IconFlag flag={lang.flag} />
                    </DialogLineCheckbox>
                ))
            }
        </Dialog>
    )
}

const LocaleDialog = connect(state => ({
    locale: getLocale(state),
}), {
    setLocale,
})(LocaleDialogBody)

LocaleDialogBody.propTypes = propTypes
LocaleDialog.propTypes = propTypes

export default LocaleDialog

import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { useIntl } from 'react-intl'

import {
    getTransportsIconsVisible,
    setTransportsIconsVisible,
    getTransportsNumberVisible,
    setTransportsNumberVisible,
    getThemeColor,
    getThemeModifier,
    setTheme,
    getTableHeaderVisible,
    setTableHeaderVisible,
} from '../../store/commonSlice'
import { DialogLineCheckbox, Dialog } from '../../components/Dialog'
import { IconGlasses, IconNumberView, IconTable, IconWheelchairSquare } from '../../resources/icons'
import { THEME_COLOR, THEME_MODIFIER } from '../../resources/config/theme-constants'


const propTypes = {
    opened: PropTypes.bool.isRequired,
    handleCloseDialog: PropTypes.func.isRequired,
}

/* eslint react/prop-types: ["error", { ignore: [transportsIconsVisible, setTransportsIconsVisible, transportsNumberVisible, setTransportsNumberVisible, themeColor, themeModifier, setTheme, tableHeaderVisible, setTableHeaderVisible] }] */
const LocaleDialogBody = ({
    opened,
    handleCloseDialog,
    transportsIconsVisible,
    setTransportsIconsVisible,
    transportsNumberVisible,
    setTransportsNumberVisible,
    themeColor,
    themeModifier,
    setTheme,
    tableHeaderVisible,
    setTableHeaderVisible,
}) => {
    const intl = useIntl()

    const toggleTransportsIconsVisibleHandler = useCallback(() => {
        setTransportsIconsVisible(!transportsIconsVisible)
    }, [transportsIconsVisible, setTransportsIconsVisible])

    const toggleTransportsNumberVisibleHandler = useCallback(() => {
        setTransportsNumberVisible(!transportsNumberVisible)
    }, [transportsNumberVisible, setTransportsNumberVisible])

    const isLightTheme = themeColor === THEME_COLOR.light && themeModifier === THEME_MODIFIER.normal
    const toggleThemeHandler = useCallback(() => {
        if (isLightTheme) {
            setTheme({ themeColor: THEME_COLOR.dark, themeModifier: THEME_MODIFIER.normal })
        } else {
            setTheme({ themeColor: THEME_COLOR.light, themeModifier: THEME_MODIFIER.normal })
        }
    }, [isLightTheme, setTheme])

    const toggleTableHeaderVisibleHandler = useCallback(() => {
        setTableHeaderVisible(!tableHeaderVisible)
    }, [tableHeaderVisible, setTableHeaderVisible])

    if (!opened) {
        return null
    }

    return (
        <Dialog
            opened
            handleCloseDialog={handleCloseDialog}
            title={intl.formatMessage({ id: 'pages.stationForecasts.footer.menu.settings' })}
        >
            <DialogLineCheckbox
                checked={transportsIconsVisible}
                title={intl.formatMessage({ id: 'pages.stationForecasts.popup.settings.showTags' })}
                handleCheck={toggleTransportsIconsVisibleHandler}
            >
                <IconWheelchairSquare />
            </DialogLineCheckbox>
            <DialogLineCheckbox
                checked={transportsNumberVisible}
                title={intl.formatMessage({ id: 'pages.stationForecasts.popup.settings.showGosNumber' })}
                handleCheck={toggleTransportsNumberVisibleHandler}
            >
                <IconNumberView />
            </DialogLineCheckbox>
            <DialogLineCheckbox
                checked={!isLightTheme}
                title={intl.formatMessage({ id: 'pages.stationForecasts.popup.settings.contrastMode' })}
                handleCheck={toggleThemeHandler}
            >
                <IconGlasses />
            </DialogLineCheckbox>
            <DialogLineCheckbox
                checked={tableHeaderVisible}
                title={intl.formatMessage({ id: 'pages.stationForecasts.popup.settings.showTableHeader' })}
                handleCheck={toggleTableHeaderVisibleHandler}
            >
                <IconTable />
            </DialogLineCheckbox>
        </Dialog>
    )
}

const LocaleDialog = connect(state => ({
    transportsIconsVisible: getTransportsIconsVisible(state),
    transportsNumberVisible: getTransportsNumberVisible(state),
    themeColor: getThemeColor(state),
    themeModifier: getThemeModifier(state),
    tableHeaderVisible: getTableHeaderVisible(state),
}), {
    setTransportsIconsVisible,
    setTransportsNumberVisible,
    setTheme,
    setTableHeaderVisible,
})(LocaleDialogBody)

LocaleDialogBody.propTypes = propTypes
LocaleDialog.propTypes = propTypes

export default LocaleDialog

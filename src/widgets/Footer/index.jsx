import React, { useState, useCallback } from 'react'
import { useIntl } from 'react-intl'

import { IconSetup } from '../../resources/icons'

import SettingsDialog from './SettingsDialog'
import './Footer.scss'


/* eslint react/prop-types: ["off"] */
const Footer = () => {
    const intl = useIntl()

    // Settings dialog
    const [settingsDialogOpened, setSettingsDialogOpened] = useState(false)

    const handleOpenSettingsDialog = useCallback(() => {
        setSettingsDialogOpened(true)
    }, [setSettingsDialogOpened])

    const handleCloseSettingsDialog = useCallback(() => {
        setSettingsDialogOpened(false)
    }, [setSettingsDialogOpened])

    return (
        <div className="footer-wrapper">
            <div onClick={handleOpenSettingsDialog} className="menu-button" aria-hidden>
                <IconSetup />
                <div className="menu-button-label">
                    {intl.formatMessage({
                        id: 'pages.stationForecasts.footer.menu.settings',
                    })}
                </div>
            </div>
            <SettingsDialog
                opened={settingsDialogOpened}
                handleCloseDialog={handleCloseSettingsDialog}
            />
        </div>
    )
}

export default Footer

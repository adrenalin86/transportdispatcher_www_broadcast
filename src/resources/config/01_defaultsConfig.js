import { LANG_MAP } from '../lang'
import { TRANSPORT_COLUMN_DESC_VIEW } from '../../pages/StationForecastPage/Grid/constants'

import { THEME_COLOR, THEME_MODIFIER } from './theme-constants'
import { getDeepPathValueMap } from './utils'


// 1. Структура данных должна совпадать со структурой стейта стора
// 2. Массивы и их содержимое не мержится а заменяется

const defaultsConfig = {
    common: {
        locale:'ru',//пока игнорируем этот параметр
        themeName:'',//пока игнорируем этот параметр
    },
    pages: {
        displayModelsEditor:{
            displayModelsGrid:{
                dxOptions:{ //здесь можем переопределять конфигурацию компонента dxDataGrid(https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Configuration/)
                    filterRow: {visible:true}, // например можем включить строку с фильтром
                    highlightChanges: true //попросим компонент подсвечивать поля, которые изменились
                },
                popupDisplayModelEditor:{
                    dxOptions:{}//здесь можем переопределять конфигурацию компонента dxPopup(https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxPopup/Configuration/)
                }
            },
        },
        
    }
}

export const defaultsConfigMap = getDeepPathValueMap(defaultsConfig)
const availablePaths = Object.keys(defaultsConfigMap)

/**
 * Создает новый map, в который копируются только значения доступные в defaults
 * @param mapToCheck - map'а, значения которой чистим
 * @returns {{}} новый map
 */
export const cleanUnavailableFields = mapToCheck => (
    availablePaths.reduce((cleanedMap, path) => {
        if (mapToCheck[path] !== undefined) {
            cleanedMap[path] = mapToCheck[path]
        }

        return cleanedMap
    }, {})
)

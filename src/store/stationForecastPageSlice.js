import axios from 'axios'
import { createSlice, createSelector } from '@reduxjs/toolkit'
import { push as navigateTo } from 'connected-react-router'
import mapValues from 'lodash-es/mapValues'

import config from '../resources/config'
import { testForecastData, testStationInfo } from '../resources/test/stationForecastTestData'

export const STATION_FORECAST_PAGE_REDUCER_KEY = 'stationForecastPage'

const stationForecastPageSlice = createSlice({
    name: STATION_FORECAST_PAGE_REDUCER_KEY,
    initialState: {
        stationId: null,
        stationInfo: null,
        exceptTransportType: config.pages[STATION_FORECAST_PAGE_REDUCER_KEY].exceptTransportType,
        goalZoneIds: config.pages[STATION_FORECAST_PAGE_REDUCER_KEY].goalZoneIds,
        transportColumnDescView: config.pages[STATION_FORECAST_PAGE_REDUCER_KEY].transportColumnDescView,
        forecastsPending: false,
        forecasts: null,
        isTest: false,
    },
    reducers: {
        pageOpened: (state, { payload: { stationId, isTest } }) => {
            state.stationId = stationId
            state.stationInfo = null
            state.isTest = isTest

            return state
        },
        pageClosed: (state) => {
            state.stationId = null
            state.stationInfo = null
            state.isTest = false

            return state
        },
        setStationInfo: (state, { payload }) => {
            state.stationInfo = payload

            return state
        },
        setExceptTransportType: (state, { payload }) => {
            state.exceptTransportType = payload

            return state
        },
        setGoalZoneIds: (state, { payload }) => {
            state.goalZoneIds = payload

            return state
        },
        setTransportColumnDescView: (state, { payload }) => {
            state.transportColumnDescView = payload

            return state
        },
        setForecastsPending: (state) => {
            state.forecastsPending = true

            return state
        },
        forecastsFetchFail: (state) => {
            state.forecastsPending = false
            state.forecasts = null

            return state
        },
        setForecasts: (state, { payload }) => {
            state.forecastsPending = false
            state.forecasts = payload

            return state
        },
    },
})

export const fetchStationInfo = () => async (dispatch, getState) => {
    let stationId
    const state = getState()

    if (getIsTest(state)) {
        dispatch(setStationInfo(testStationInfo))
    } else {
        try {
            stationId = getStationId(state)

            const { data } = await axios.get('/getStationInfo.php', { params: { sid: stationId } })

            data.zones = mapValues(data.zones, (zone) => {
                zone.zone_id = zone.zone_id.toString()
                zone.zonecategory_id = zone.zonecategory_id.toString()
                zone.route_ids = zone.route_ids.map(routeId => routeId.toString())

                return zone
            })

            const currentState = getState()
            const currentStationId = getStationId(currentState)

            if (currentStationId === stationId) {
                if (data && data.name && data.name.length) {
                    dispatch(setStationInfo(data))
                } else if (stationId !== '76') {
                    // Нужна проверка для предотвращения зацикливания, т.к. 76 это дефолтный id при открытии
                    // eslint-disable-next-line no-console
                    console.error(`Остановка с id="${stationId}" не найдена`)
                    dispatch(navigateTo('/'))
                }
            }
        } catch (e) {
            // eslint-disable-next-line no-console
            console.error(`Ошибка получения остановки с id="${stationId}": `, e)
            dispatch(navigateTo('/'))
        }
    }
}

export const fetchForecasts = () => async (dispatch, getState) => {
    const state = getState()

    // если страница для теста, выводим данные по прогнозу из файла
    if (getIsTest(state)) {
        dispatch(stationForecastPageSlice.actions.setForecasts(testForecastData))
    } else {
        try {
            const stationId = getStationId(state)
            const pending = getForecastsPending(state)

            if (stationId === null || pending) {
                return
            }

            dispatch(stationForecastPageSlice.actions.setForecastsPending())

            const { data } = await axios.get('/getStationForecastsWithTags.php', {
                params: { sid: stationId },
            })

            data.forEach((forecast) => {
                forecast.obj_tags = forecast.obj_tags.map(objTag => objTag.toString())
            })

            dispatch(stationForecastPageSlice.actions.setForecasts(data))
        } catch (e) {
            // eslint-disable-next-line no-console
            console.error(e)
            dispatch(stationForecastPageSlice.actions.forecastsFetchFail(e.toString()))
        }
    }
}

export const {
    pageOpened,
    pageClosed,
    setStationInfo,
    setExceptTransportType,
    setGoalZoneIds,
    setTransportColumnDescView,
    forecastsFetchFail,
    setForecasts,
} = stationForecastPageSlice.actions

export const getStationForecastPage = state => state.pages[STATION_FORECAST_PAGE_REDUCER_KEY]
export const getStationId = state => getStationForecastPage(state).stationId
export const getIsTest = state => getStationForecastPage(state).isTest
export const getStationInfo = state => getStationForecastPage(state).stationInfo
export const getExceptTransportType = state => getStationForecastPage(state).exceptTransportType
export const getGoalZoneIds = state => getStationForecastPage(state).goalZoneIds
export const getTransportColumnDescView = state => getStationForecastPage(state).transportColumnDescView
export const getForecastsPending = state => getStationForecastPage(state).forecastsPending
export const getForecasts = state => getStationForecastPage(state).forecasts
export const getVisibleForecasts = createSelector(
    [getStationInfo, getExceptTransportType, getGoalZoneIds, getForecasts],
    (stationInfo, exceptTransportType, goalZoneIds, forecasts) => {
        if (!stationInfo || !forecasts) {
            return []
        }

        // Visible route types

        const visibleRouteTypes = stationInfo.routeTypes
            .map(routeType => routeType.short_name)
            .filter(routeType => !exceptTransportType.includes(routeType))

        // Visible route ids

        let visibleRouteIds = []

        if (goalZoneIds && goalZoneIds.length) {
            visibleRouteIds = goalZoneIds.reduce((visibleRouteIds, goalZoneId) => {
                if (stationInfo.zones[goalZoneId]) {
                    const routeIds = stationInfo.zones[goalZoneId].route_ids

                    routeIds.forEach((routeId) => {
                        if (!visibleRouteIds.includes(routeId)) {
                            visibleRouteIds.push(routeId)
                        }
                    })
                }

                return visibleRouteIds
            }, [])
        }

        return forecasts
            .filter(({ rtype }) => visibleRouteTypes.includes(rtype))
            .filter(({ rid }) => !visibleRouteIds.length || visibleRouteIds.includes(rid))
    },
)

export default stationForecastPageSlice.reducer

import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { reduxBatch } from '@manaflair/redux-batch'
import { connectRouter, routerMiddleware } from 'connected-react-router'

import { history } from '../services/history'

import commonReducer, { COMMON_REDUCER_KEY } from './commonSlice'
import stationForecastPageReducer, { STATION_FORECAST_PAGE_REDUCER_KEY } from './stationForecastPageSlice'
import stationSearchByTypePageSlice, { STATION_SEARCH_BY_TYPE_PAGE_REDUCER_KEY } from './stationSearchByTypePageSlice'
import { saveUserConfigMiddleware } from './saveUserConfigMiddleware'
import { stationForecastPageMiddleware } from './stationForecastPageMiddleware'
import stationSearchByKeywordPageSlice, {
    STATION_SEARCH_BY_KEYWORD_PAGE_REDUCER_KEY,
} from './stationSearchByKeywordPageSlice'
import stationSearchByLocationPageSlice, {
    STATION_SEARCH_BY_LOCATION_PAGE_REDUCER_KEY,
} from './stationSearchByLocationPageSlice'


const rootReducer = combineReducers({
    router: connectRouter(history),
    [COMMON_REDUCER_KEY]: commonReducer,
    pages: combineReducers({
        [STATION_FORECAST_PAGE_REDUCER_KEY]: stationForecastPageReducer,
        [STATION_SEARCH_BY_TYPE_PAGE_REDUCER_KEY]: stationSearchByTypePageSlice,
        [STATION_SEARCH_BY_KEYWORD_PAGE_REDUCER_KEY]: stationSearchByKeywordPageSlice,
        [STATION_SEARCH_BY_LOCATION_PAGE_REDUCER_KEY]: stationSearchByLocationPageSlice,
    }),
})

const store = configureStore({
    reducer: rootReducer,
    middleware: [
        ...getDefaultMiddleware(),
        routerMiddleware(history),
        saveUserConfigMiddleware,
        stationForecastPageMiddleware,
    ],
    enhancers: [reduxBatch],
})

export default store

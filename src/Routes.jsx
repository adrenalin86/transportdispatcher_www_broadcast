import React from 'react'
import { Switch, Route } from 'react-router-dom'

import HomePage from './pages/HomePage'
import StationForecastsPage from './pages/StationForecastPage'

export default () => (
    <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/station_forecasts" component={StationForecastsPage} />

        {/* <Route>
            <Redirect to="/" />
        </Route> */}
    </Switch>
)
